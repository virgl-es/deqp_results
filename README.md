This repo contains results for dEQP test runs.

The result file names follow the following scheme:

**deqp-gles2-...**: all dEQP-GLES2.\* tests  
**cts-gles2-...**: all tests that the cts-runner runs  
**aosp-gles2-...**: tests in aosp_mustpass/../gles2-master.txt  
**android-gles2-...**: tests in android/cts/master/gles2-master.txt  

**...-on-gl-[host-driver]**: Running on a host using desktop OpenGL  
**...-on-gles-[host-driver]**: Running on a host using OpenGL ES  
